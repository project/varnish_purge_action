<?php

/**
 * @file
 * Varnish Purge Action module.
 */

/**
 * Implement hook_rules_action_info().
 */
function varnish_purge_action_rules_action_info() {
  return array(
    'varnish_purge_action_node' => array(
      'label' => t('Purge Varnish Page Cache for Node'),
      'group' => t('Custom'),
      'parameter' => array(
        'string' => array(
          'type' => 'text',
          'label' => t('Page to clear'),
          'description' => t('Enter the nid of the page to be cleared'),
        ),
      ),
    ),
    'varnish_purge_action_term' => array(
      'label' => t('Purge Varnish Page Cache for Term'),
      'group' => t('Custom'),
      'parameter' => array(
        'string' => array(
          'type' => 'text',
          'label' => t('Page to clear'),
          'description' => t('Enter the tid of the page to be cleared'),
        ),
      ),
    ),
  );
}

/**
 * Action to purge cache.
 *
 * @param int $nid
 *   Function varnish purge action node nid.
 */
function varnish_purge_action_node($nid) {
  $uri = check_url('node/' . $nid);

  // Clear cache for the uri.
  varnish_purge($_SERVER['HTTP_HOST'], $uri);
  varnish_purge($_SERVER['HTTP_HOST'], drupal_get_path_alias($uri));

  // Set confirmation message.
  drupal_set_message('A purge request has been sent to Varnish for this page.');
}

/**
 * Action to purge cache.
 *
 * @param int $tid
 *   Function purge action term tid.
 */
function varnish_purge_action_term($tid) {
  $uri = check_url('taxonomy/term/' . $tid);

  // Clear cache for the uri.
  varnish_purge($_SERVER['HTTP_HOST'], $uri);
  varnish_purge($_SERVER['HTTP_HOST'], drupal_get_path_alias($uri));

  // Set confirmation message.
  drupal_set_message('A purge request has been sent to Varnish for this term.');
}
